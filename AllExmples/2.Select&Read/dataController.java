package asdf;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "obj")
@SessionScoped
public class dataController 
{
    private List<employee>employees = null;
    private employee selectRow = null;
    
    
    public void select(employee e)
    {
        selectRow = e;
    }
    
    public employee getSelectRow() {
        return selectRow;
    }

    public void setSelectRow(employee selectRow) {
        this.selectRow = selectRow;
    }
    
    
    
    
    
    
    
    
    
    @PostConstruct
    public void init()
    {
        employees = new ArrayList<employee>();
        
        employees.add(new employee("Rijwan", "07"));
        employees.add(new employee("Solaiman", "15"));
        employees.add(new employee("Arif", "33"));
        
        selectRow = new employee();
    }

    public List<employee> getEmployees() 
    {
        return employees;
    }

    public void setEmployees(List<employee> employees) 
    {
        this.employees = employees;
    }
    
    
}
