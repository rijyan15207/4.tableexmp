package asdf;

public class employee 
{
    private String name;
    private String id;
    
    // Constructor
    
    // parameterized constructor
    public employee(String name, String id) {
        this.name = name;
        this.id = id;
    }
    
    // zero parameter
    public employee() {}
    
    
    // Setter & Getter

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
    // toString

    @Override
    public String toString() {
        return "employee{" + "name = " + name + ", id = " + id + '}';
    }
    
}
